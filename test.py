from genie.conf import Genie
from genie.testbed import load
from pyats.topology import loader
from ruamel.yaml import YAML
import yaml

f=open('./inventory.yml','r')
doc=yaml.load(f)
doc['devices']['CE1']['os']='Mooo'
doc['devices']['MO']=doc['devices'].pop('CE1')

print(doc)

fc=open('./inventory1.yml','w')
yaml.safe_dump(doc,fc)


 

testbed=loader.load('./inventory1.yml')

for device in testbed.devices.values():
    print(f'Connecting to {device.name}')