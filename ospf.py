
import json
from textwrap import indent
from genie.conf import Genie
from genie.testbed import load
from pprint import pprint


testbed=load("./inventory.yml")
tb = Genie.init(testbed)
for device in testbed.devices.values():
    print(f'Connecting to {device.name}')
    
    try:
        device.connect(log_stdout=False)
        out= device.learn('ospf').info
        p=out['vrf']['default']['address_family']['ipv4']['instance']['1']['areas']['0.0.0.0']['interfaces']
        for k, v in p.items():
            if "neighbors" in v.keys():
                for nei,data in v['neighbors'].items():
                    print(f"{device.name} has OSPF neighbor with {data['neighbor_router_id']} on {k}  {nei}")
        # p=out['instance']['default']['vrf']['default']
   
     
        # if "neighbor" in p.keys():
        #     for nei ,data in p['neighbor'].items():
        #         print(f"{device.name} has OSPF neighbor with nei {nei}  {data['session_state']}")
    except:
        print('Null')